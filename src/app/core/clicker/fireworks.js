//spaghetti.js
var canvas = document.getElementById('c'),
    ctx = canvas.getContext('2d'),
    cw = canvas.width,
    ch = canvas.height,
    radius = 1;
    friction=0.995,
    balls = [];

function bomb() {
    rand  = Math.random() * 2 - 1;
    rand2 = Math.random() * 2 + 2;
    rand3 = Math.random() * 2 + 2;
    rand4 = Math.random() * 15 + 5;
    pos   = Math.random() * 300 - 150;

    for(x = 0; x < Math.random() * 30 + 10; x++) {
        for(y = 0; y < rand4; y++) {
            balls.push([cw/2+100+pos-x*3, 100+y*3, -rand2-1, -0.1-rand2/120, 1, 10,Math.round(Math.random()*90+165), Math.round(Math.random()*100),Math.round(Math.random()*100),0]);
            balls.push([cw/2-100+pos-x*3, 101+y*3,  rand3+1, -0.1-rand3/100, 1, 10,Math.round(Math.random()*127+127),Math.round(Math.random()*150),Math.round(Math.random()*50),0]);
        }
    }

}

//from billiard.js
function ball_collision() {
    //bombs gonna bomb
    if(balls.length<50)bomb();

    for(i = 0; i < balls.length; i++) {
        for(j = i + 1; j < balls.length; j++) {
            //simple collision check first
            if(balls[i][0] + radius + radius > balls[j][0] && balls[i][0] < balls[j][0] + radius + radius && balls[i][1] + radius + radius > balls[j][1] && balls[i][1] < balls[j][1] + radius + radius) {
                distance = Math.sqrt((balls[i][0] - balls[j][0]) * (balls[i][0] - balls[j][0]) + (balls[i][1] - balls[j][1]) * (balls[i][1] - balls[j][1]));
                if(distance < radius + radius) {
                    balls[i][9] = 1;
                    balls[j][9] = 1;

                    collision = Math.atan2(balls[i][1] - balls[j][1], balls[i][0] - balls[j][0]);

                    magni_1 = Math.sqrt(balls[i][2] * balls[i][2] + balls[i][3] * balls[i][3]);
                    magni_2 = Math.sqrt(balls[j][2] * balls[j][2] + balls[j][3] * balls[j][3]);

                    dir_1 = Math.atan2(balls[i][3], balls[i][2]);
                    dir_2 = Math.atan2(balls[j][3], balls[j][2]);

                    nxspeed_1 = Math.sqrt(balls[i][2] * balls[i][2] + balls[i][3] * balls[i][3]) * Math.cos(dir_1 - collision);
                    nyspeed_1 = Math.sqrt(balls[i][2] * balls[i][2] + balls[i][3] * balls[i][3]) * Math.sin(dir_1 - collision);

                    nxspeed_2 = magni_2 * Math.cos(dir_2 - collision);
                    nyspeed_2 = magni_2 * Math.sin(dir_2 - collision);

                    fxspeed_1 = ((balls[i][5] - balls[j][5]) * nxspeed_1 + (balls[j][5] + balls[j][5]) * nxspeed_2) / (balls[i][5] + balls[j][5]);
                    fxspeed_2 = ((balls[i][5] + balls[i][5]) * nxspeed_1 + (balls[j][5] - balls[i][5]) * nxspeed_2) / (balls[i][5] + balls[j][5]);

                    final_yspeed_1 = nyspeed_1;
                    final_yspeed_2 = nyspeed_2;

                    balls[i][2] = Math.cos(collision) * fxspeed_1 + Math.cos(collision + Math.PI / 2) * final_yspeed_1;
                    balls[i][3] = Math.sin(collision) * fxspeed_1 + Math.sin(collision + Math.PI / 2) * final_yspeed_1;
                    balls[j][2] = Math.cos(collision) * fxspeed_2 + Math.cos(collision + Math.PI / 2) * final_yspeed_2;
                    balls[j][3] = Math.sin(collision) * fxspeed_2 + Math.sin(collision + Math.PI / 2) * final_yspeed_2;
                }
            }
        }
    }
}

function move_balls() {
    for(i=0;i<balls.length;i++) {
        vx = balls[i][2];
        vy = balls[i][3] + 0.02;

        nx = balls[i][0] + vx;
        ny = balls[i][1] + vy;

        balls[i][0] = nx;
        balls[i][1] = ny;
        balls[i][2] = vx * friction;
        balls[i][3] = vy * friction;

        //remove out of screen
        if(nx > cw-radius || nx < radius || ny < radius)balls.splice(i,1)
    }
}

function drawballs() {
    for(i = 0; i < balls.length; i++) {
        alpha = 1 - balls[i][1] / 475;

        if(balls[i][9]==0)alpha=0;

        ctx.fillStyle="rgba("+balls[i][6]+","+balls[i][7]+","+balls[i][8]+","+alpha+")";
        ctx.beginPath();
        ctx.arc(balls[i][0], balls[i][1], radius, 0, 2 * Math.PI, false);

        ctx.fill();
        if(alpha<0)balls.splice(i,1)

    }


    imgdata = ctx.getImageData(0 , 0, cw, ch);
    pixels = imgdata.data;
    for(y = 1; y < ch - 5; y++) {
        for(x = 0; x < cw; x++) {
            curpix = y * cw * 4 + x * 4;
            pixels[curpix - cw*4 +3] = pixels[curpix - cw * 4 +3] - 3;
        }
    }
    ctx.putImageData(imgdata, 0, 0);
    
}

function render() {
    move_balls();
    ball_collision();
    drawballs();
    requestAnimationFrame(render);
}
render();