/**
 * Gameplay Instructions:
 * A new circle is drawn immediately after you click the previous circle
 * You have 1.5 seconds to click the next circle, otherwise the game ends
 */

// Contain everything for this game in an object
var game = {
  
  // Start the score off at 0
  score: 0,
  
  // Adds 1 point to the current score
  incrementScore: function() {
    var mensagens = [
    'Você está indo muito bem!',
    'Você é realmente bom nisso!',
    'Eu queria ter toda essa habilidade!',
    'Você já fez isso antes, né?',
    'Você nasceu pra isso.',
    'Mais 5 pontos pra conta!'
    ];
    var random = Math.floor(Math.random()*mensagens.length);
    this.score++;
    if(this.score % 5 == 0){
      var msg = new SpeechSynthesisUtterance(mensagens[random]);
      msg.pitch = 1;
      msg.lang = 'pt-BR';
      window.speechSynthesis.speak(msg);
      if(this.score == 10){
        var msg = new SpeechSynthesisUtterance("Parabéns, você venceu!");
        msg.pitch = 1;
        msg.lang = 'pt-BR';
        window.speechSynthesis.speak(msg);
        $('#canvas').css("display", "none");
        $('#ending').css("display", "block");
        $('body').css("background", "#222");
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'fireworks.js';

        document.getElementsByTagName('head')[0].appendChild(script);

        setTimeout(function(){ window.history.back(); }, 5000);
      }
    }
    // You can check what the new score is here, and possibly give out achievements
  },
  
  // Called when the game is over. A good place for achievements and high scores (and posting to FB/Twitter / taking a screenshot), etc... Score is this.score
  endGame: function() {
    this.paused = true;
    this.timeLeft = 0;
   
    // Hide the circle for 1.5 seconds, then let them start over
    this.circleRadius = 0;
    setTimeout( function() {
      game.updateCirclePosition();
    }, 5000 );
  },
  
  // Get a reference to the canvas element
  canvas: document.getElementById( 'canvas' ),
  
  // Get a reference to the context (you won't be using this, but it lets you draw to the canvas)
  ctx: canvas.getContext( '2d' ),
  
  // Start the game off in paused mode
  paused: true,
  
  color: '#000',
  colors: ['blue', 'red', 'yellow', 'green'],
  
  // Starts up the game rendering
  start: function() {
    // If you want to use the Clay.io data storage feature, say, to save the player's score (even if they refresh the page), you would do that in here, and set the score (this.score) based on the data, then call this.loop() inside the Clay.io API call callback instead of right away. This gets pretty complicated though, not because of Clay, but because of JavaScript :)
    this.addInputListeners();
    this.loop();
  },

  // This is called 60 times a second, before each rendering
  loop: function() {
   
    // Check if the mouse is down, and has clicked a circle
    if( this.checkIfClicked() )
    {
      // The game is paused if it's the start of a game, or if a game previously ended
      if( this.paused == true )
      {
        // Start a new game
        this.lastTime = ( new Date() ).getTime();
        this.score = 0;
        this.paused = false;
      }
      
      $('#game-title').css("display", "none");

      // Give the player another point
      this.incrementScore();

      
      // Add another 1.5 seconds
      this.timeLeft = 60000;
      
      // Move the circle to a new place
      this.updateCirclePosition();
    }
    
    
    // Checks if the player has run out of time or not (only if they have started the game)
    if( ! this.paused && this.checkIfTimeLeft() === false ) 
    {
      // if the above if statement evaluates to true, that means they are out of time
      this.endGame();
    }
    this.render();
    
    // Continually call this same function as fast as possible (ideally 60 times per second)
    window.requestAnimationFrame( this.loop.bind( this ) );
  },

  // Called from the game loop, renders elements to the screen  
  render: function() {
    // Clear the entire canvas before redrawing
    this.ctx.clearRect( 0, 0, this.canvas.width, this.canvas.height );
    this.ctx.fillText( 'Pontuação: ' + this.score, 10, 20 );
    
    // Get the width of the time area
    var timeText = ' ';
    var timeTextWidth = this.ctx.measureText( timeText ).width;
    this.ctx.fillText( timeText, this.canvas.width - timeTextWidth - 10, 20 );
    
    // If the game is paused, show instructions
    if( this.paused )
    {
      var instructions = 'Clique no Ghostinho para iniciar';
      var instructionsWidth = this.ctx.measureText( instructions ).width;
      this.ctx.fillText( instructions, this.canvas.width / 2 - instructionsWidth / 2, 20 );
      this.ctx.font = '25px verdana';
    }
    
    // This draws a circle with a center at coordinates (circleX, circleY) and radius of circleRadius
    this.ctx.beginPath();
    this.ctx.arc( this.circleX, this.circleY, this.circleRadius, 0, 2 * Math.PI, false );
    // This fills in the circle
    //
    var c = document.getElementById( 'canvas' );
    var ctx = c.getContext('2d');
    var img = new Image();
    img.src = "ghost-happy.png";
    ctx.drawImage(img,this.circleX-this.circleRadius,this.circleY - this.circleRadius, this.circleRadius * 2, this.circleRadius * 2);
  },
  
  // Time that we last checked the time
  lastTime: ( new Date() ).getTime(),
  
  // The time left in milliseconds (1.5 seconds)
  timeLeft: 60000,
  
  // Check if the user still has time left
  checkIfTimeLeft: function() {
    var currentTime = ( new Date() ).getTime();
  
    // How many milliseconds have passed since we last checked the time
    var dt = currentTime - this.lastTime;
  
    // Remove how many milliseconds have passed from the game clock
    this.timeLeft -= dt;
  
    // Set the new last time
    this.lastTime = currentTime;
  
    if( this.timeLeft > 0 )
      return true; // there is still time left
    else
      return false; // the player has run out of time
    },
  
  // Check if a circle is being clicked
  checkIfClicked: function() {
    if( this.mouseDown )
    {
      // Mark the mouse button as no longer down
      this.mouseDown = false;
    
      // Mouse button is down, check if it's clicking within the bounds of the circle
      var xBounded = Math.abs( this.mouseX - this.circleX ) < this.circleRadius;
      var yBounded = Math.abs( this.mouseY - this.circleY ) < this.circleRadius;
      if( xBounded && yBounded )
      {
        return true;
      }
    }
    return false; // not being clicked
  },
  
  // Center of circle in x direction
  circleX: 200,
  // Center of circle in x direction
  circleY: 200,
  // Radius of circle
  circleRadius: 75,
  
  updateCirclePosition: function() {
    // Make the game progressively get harder (the higher the score, the smaller the minimum circle
    var minRadius = 50 - this.score;
    if( minRadius < 3 )
      minRadius = 45; // absolute minimum is 3px
    
    var colorIndex = Math.floor( this.colors.length * Math.random() );
    
    // between minRadius and 50px + minRadius
    this.circleRadius = minRadius + Math.random() * 100
     // between the radius and the canvas width - radius
    this.circleX = this.circleRadius + Math.random() * ( canvas.width - 2 * this.circleRadius );
     // between the radius and the canvas height - radius
    this.circleY = this.circleRadius + Math.random() * ( canvas.height - 2 * this.circleRadius );
  },
  
  // This is where we store information about the mouse state (position and whether the mouse button is clicked
  mouseX: 0,
  mouseY: 0,
  mouseDown: false,
  
  addInputListeners: function() {
    // Start listening for input. For this game, it's just mouse input
    canvas.addEventListener( 'mousemove', function( e ) {
      // Update the x and y positions of the mouse so we can compare against the circle position later
      game.mouseX = typeof e.offsetX === 'undefined' ? e.layerX : e.offsetX;
      game.mouseY = typeof e.offsetY === 'undefined' ? e.layerY : e.offsetY;
    } );
    canvas.addEventListener( 'mousedown', function( e ) {
      // Store that the mouse button has been clicked
      game.mouseDown = true;
    } );
  }
};
  
$('#ghost').click(function(){

  $('#canvas').css("display", "block");
  var canvass = document.getElementById("canvas");
  canvass.style = "padding:5px;display:block; margin:0 auto;";
  $('#ghost').css("display", "none");
  $('#game-title').css("display", "block");
  $('#ghost-title').css("display", "none");
  game.start();   
})

var msg = new SpeechSynthesisUtterance("Olá! Você pode me acompanhar nesse jogo?");
msg.pitch = 1;
msg.lang = 'pt-BR';
window.speechSynthesis.speak(msg);

