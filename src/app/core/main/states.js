define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = [ '$urlRouterProvider', '$stateProvider' ];

  function configureStates( $urlRouterProvider, $stateProvider ) {

    $urlRouterProvider
      .when('', '/aluno')
      .when('/', '/aluno')
      .otherwise("/aluno");

      $stateProvider
          .state('home', {
              url: '/home',
              views: {
                  'master': {
                      templateUrl   : 'app/core/layout/templates/master.html'
                  },
                  'content@home': {
                      templateUrl   : 'app/core/main/templates/home.html',
                      controller    : 'MainCtrl',
                      controllerAs  : 'vm'
                  }
              }
          });

  }

});
